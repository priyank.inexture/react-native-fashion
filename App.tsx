import "react-native-gesture-handler";
import * as React from "react";
import { useFonts } from "expo-font";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";

import Onboarding from "./src/Authentication";
const AuthenticationStack = createStackNavigator();
const AuthenticationNavigator = () => {
  return (
    <AuthenticationStack.Navigator headerMode="none">
      <AuthenticationStack.Screen name="Onboarding" component={Onboarding} />
    </AuthenticationStack.Navigator>
  );
};
export default function App() {
  const [loaded] = useFonts({
    sfBold: require("./assets/fonts/SF-Pro-Display-Bold.otf"),
    sfRegular: require("./assets/fonts/SF-Pro-Display-Regular.otf"),
    sfSemiBold: require("./assets/fonts/SF-Pro-Display-Semibold.otf"),
  });

  if (!loaded) {
    return null;
  }

  return (
    <NavigationContainer>
      <AuthenticationNavigator />
    </NavigationContainer>
  );
}
