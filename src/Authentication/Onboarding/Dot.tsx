import React from "react";
import { StyleSheet, Animated } from "react-native";
interface DotProps {
  index: number;
  // scrollX: number;
  currentIndex: Animated.AnimatedDivision;
}
// const { height } = Dimensions.get("window");
// export const SLIDE_HEIGHT = 0.61 * height;
const Dot = ({
  index,
  //  scrollX,
  currentIndex,
}: DotProps) => {
  //   const transform = [];
  const scale = currentIndex.interpolate({
    inputRange: [index - 1, index, index + 1],
    outputRange: [0.8, 1.05, 0.8],
    extrapolate: "clamp",
  });
  const opacity = currentIndex.interpolate({
    inputRange: [index - 1, index, index + 1],
    outputRange: [0.5, 1, 0.5],
    extrapolate: "clamp",
  });
  return (
    <Animated.View
      style={[{ ...styles.dot, opacity }, { transform: [{ scale }] }]}
      key={index}
    />
  );
};
const styles = StyleSheet.create({
  dot: {
    width: 10,
    marginHorizontal: 2,
    height: 10,
    backgroundColor: "#2CB9B0",
    borderRadius: 10,
  },
});
export default Dot;
