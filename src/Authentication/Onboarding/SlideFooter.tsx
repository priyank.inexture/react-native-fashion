import React from "react";
import { StyleSheet, View, Text, Dimensions } from "react-native";

import Button from "../../components/Button";
interface SlideFooterProps {
  subTitle: string;
  subDescription: string;
  onPress: () => void;
  isLast: boolean;
}
const { width, height } = Dimensions.get("window");
export const SLIDE_HEIGHT = 0.61 * height;
const SlideFooter = ({
  subTitle,
  subDescription,
  onPress,
  isLast,
}: SlideFooterProps) => {
  return (
    <View style={styles.container}>
      <Text style={styles.subTitle}>{subTitle}</Text>
      <Text style={styles.subDescription}>{subDescription}</Text>
      <Button
        varient={!isLast ? "default" : "primary"}
        title={!isLast ? "Next" : "Let's  get started"}
        onPress={onPress}
      />
    </View>
  );
};
const styles = StyleSheet.create({
  container: {
    // backgroundColor: "yellow",
    justifyContent: "center",
    alignItems: "center",
    width: width,
    paddingHorizontal: 0.1 * width,
  },
  subTitle: {
    textAlign: "center",
    fontSize: 24,
    marginBottom: 16,
    fontFamily: "sfSemiBold",
  },
  subDescription: {
    textAlign: "center",
    fontSize: 16,
    marginBottom: 16,
    fontFamily: "sfRegular",
  },
});
export default SlideFooter;
