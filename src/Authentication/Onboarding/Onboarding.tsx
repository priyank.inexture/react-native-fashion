import React, { useRef } from "react";
import { Dimensions, StyleSheet, View, Animated } from "react-native";
import { ScrollView } from "react-native-gesture-handler";

import Dot from "./Dot";
import Slide, { SLIDE_HEIGHT } from "./Slide";
import SlideFooter from "./SlideFooter";
// interface ComponentNameProps {}
const slides = [
  {
    title: "Relaxed",
    subTitle: "Find Your Outfits",
    subDescription:
      "Confused about your outfit? Don't worry! Find the best outfit here!",
    bgColor: "#BFEAFF",
  },
  {
    title: "Playfull",
    subTitle: "Hear it First, Wear it First",
    subDescription:
      "Hating the clothes in your wardrobe? Explore hundreds of outfit ideas",
    bgColor: "#BEECCF",
  },
  {
    title: "Excentric",
    subTitle: "Your Style, Your Way",
    subDescription:
      "Create your individual& unique styl and look amazing everyday",
    bgColor: "#FFE4DF",
  },
  {
    title: "Funky",
    subTitle: "Look Good, Feel Good",
    subDescription:
      "Discover the latest trends in fashion and explore your personality",
    bgColor: "#FFDDFF",
  },
];
const { width } = Dimensions.get("window");
const ComponentName = () => {
  const scrollRef = useRef<ScrollView>(null);
  const scrollX = useRef(new Animated.Value(0)).current;
  const BackgroundColorConfig = scrollX.interpolate({
    inputRange: slides.map((_, index) => index * width),
    outputRange: slides.map((s) => s.bgColor),
  });

  return (
    <View style={styles.container}>
      <Animated.View
        style={[styles.slider, { backgroundColor: BackgroundColorConfig }]}
      >
        <Animated.ScrollView
          ref={scrollRef}
          onScroll={Animated.event([
            {
              nativeEvent: {
                contentOffset: {
                  x: scrollX,
                },
              },
            },
          ])}
          horizontal
          snapToInterval={width}
          decelerationRate="fast"
          showsHorizontalScrollIndicator={false}
          bounces={false}
        >
          {slides.map((s, index) => (
            <Slide label={s.title} right={index % 2 === 1} key={index} />
          ))}
        </Animated.ScrollView>
      </Animated.View>
      <Animated.View
        style={[styles.footer, { backgroundColor: BackgroundColorConfig }]}
      >
        <View style={styles.footerContent}>
          {/* <Text>this is text</Text> */}
          {/* {console.log(scrollX)} */}
          <View style={styles.pills}>
            {slides.map((_, index) => (
              <Dot
                key={index}
                index={index}
                currentIndex={Animated.divide(scrollX, width)}
              />
            ))}
          </View>
          <Animated.View
            style={[
              styles.subSlideContainer,
              {
                transform: [{ translateX: Animated.multiply(scrollX, -1) }],
              },
            ]}
          >
            {slides.map((s, index) => (
              <SlideFooter
                key={index}
                subTitle={s.subTitle}
                isLast={index === slides.length - 1}
                subDescription={s.subDescription}
                onPress={() => {
                  if (scrollRef.current) {
                    scrollRef.current.scrollTo({
                      x: width * (index + 1),
                      animated: true,
                    });
                  }
                }}
              />
            ))}
          </Animated.View>
        </View>
      </Animated.View>
    </View>
  );
};
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "white",
  },
  slider: {
    height: SLIDE_HEIGHT,
    backgroundColor: "#BFEAF5",
    borderBottomRightRadius: 75,
  },
  footer: {
    flex: 1,
    backgroundColor: "#BFEAF5",
  },
  footerContent: {
    flex: 1,
    flexDirection: "row",
    ...StyleSheet.absoluteFillObject,
    backgroundColor: "white",
    borderTopLeftRadius: 75,
  },
  subSlideContainer: {
    flexDirection: "row",
    alignItems: "center",
  },
  pills: {
    ...StyleSheet.absoluteFillObject,
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    height: SLIDE_HEIGHT * 0.15,
    // backgroundColor: "red",
    width: width,
  },
});
export default ComponentName;
