import React from "react";
import { StyleSheet, View, Text, Dimensions } from "react-native";
import { RectButton } from "react-native-gesture-handler";
interface ButtonProps {
  onPress: () => void;
  varient?: string;
  title: string;
}
const { width } = Dimensions.get("window");
const Button = ({ onPress, varient, title }: ButtonProps) => {
  const backgroundColor =
    varient === "primary" ? "#2CB9B0" : "rgba(12,13,52,0.05)";
  return (
    <RectButton
      style={[styles.container, { backgroundColor }]}
      {...{ onPress }}
    >
      <View>
        <Text
          style={[
            styles.label,
            { color: varient === "primary" ? "white" : "black" },
          ]}
        >
          {title}
        </Text>
      </View>
    </RectButton>
  );
};
const styles = StyleSheet.create({
  container: {
    borderRadius: 25,
    height: 50,
    width: width * 0.8 * 0.9,
    justifyContent: "center",
    alignItems: "center",
  },
  label: {
    fontFamily: "sfRegular",
    fontSize: 15,
    textAlign: "center",
  },
});
export default Button;
